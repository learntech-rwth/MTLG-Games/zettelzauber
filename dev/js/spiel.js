// Erstellt den Startscreen
function start() {
    boxwidth = stage.canvas.width / 24;
    boxheight = stage.canvas.width / 24;
    abstand = stage.canvas.width / 192;
    stage.addChild(bg);

    // Textbausteine
    text1 = new createjs.Text(l("%spielbeginnPlural"), "50px Arial Black", "white");
    text2 = new createjs.Text(l("%spieler"), "30px Arial Black", "white");
    text3 = new createjs.Text(l("%frageSpieleranzahl"), "50px Arial Black", "white");
    text4 = new createjs.Text(spieleranzahl, "30px Arial Black", "#911322");
    text1.x = stage.canvas.width / 2 - text1.getMeasuredWidth() / 2;
    text1.y = (9 / 20) * stage.canvas.height - text1.getMeasuredHeight();
    text2.x = stage.canvas.width / 2 - text2.getMeasuredWidth() / 2 + text4.getMeasuredWidth() / 2;
    text2.y = (1 / 4) * stage.canvas.height - boxwidth - abstand / 2 - 2 * abstand - text2.getMeasuredHeight();
    text3.x = stage.canvas.width / 2 - text3.getMeasuredWidth() / 2;
    text3.y = (9 / 20) * stage.canvas.height - text3.getMeasuredHeight();
    text4.x = stage.canvas.width / 2 - text4.getMeasuredWidth() / 2 - text2.getMeasuredWidth() / 2;
    text4.y = (1 / 4) * stage.canvas.height - boxwidth - abstand / 2 - 2 * abstand - text4.getMeasuredHeight();
    stage.addChild(text3);

    // Dragsymbol hinzufügen
    var drag = new createjs.Bitmap("img/drag.png");
    drag.x = stage.canvas.width / 2 - 35;
    drag.y = (3 / 5) * stage.canvas.height - 50;
    stage.addChild(drag);

    // Auswahlbox (Container 0) und Spielfiguren (Container 1-4) erzeugen
    var z = 4; // Schleifenkorrektur für Container 0

    for (var i = 0; i < 5; i++) {
		container[i] = new createjs.Container();
		container[i].regX = boxwidth / 2;
		container[i].regY = boxheight + abstand / 2;
		container[i].x = (i / 5) * stage.canvas.width;
		container[i].y = (4 / 5) * stage.canvas.height;

		if(i == 0) {
			container[i].regX = 0;
			container[i].regY = 0;
			container[i].x = stage.canvas.width / 2 - boxwidth - abstand / 2;
    		container[i].y = (1 / 4) * stage.canvas.height - boxwidth - abstand / 2;
		}

		else if(i == 1) {
			container[i].regY = boxheight / 2;
			container[i].y = (4 / 5) * stage.canvas.height - boxwidth / 2 - abstand / 2;
			z = 0;
		}

		else if(i > 2) {
			container[i].regX = boxwidth + abstand / 2;
		}

		// Boxen in die Container einfügen
		for (var j = 0; j < i+z; j++) {
            box = new createjs.Shape();
            randomBit = Math.floor(Math.random() * 2);
            box.shadow = new createjs.Shadow("#4C4C4C", 3, 3, 8);

            if (j % 2 != 0) {
                box.y = boxheight + abstand;
            }

            if (j > 1) {
                box.x = boxwidth + abstand;
           	}

            if(i == 0) {
            	box.graphics.setStrokeStyle(4).beginStroke('#4C4C4C').beginFill("white").drawRoundRect(0, 0, boxwidth, boxheight, 6, 6);
            	box.alpha = 0.2;
            	auswahlboxen[j] = box;
        		container[i].addChild(auswahlboxen[j]);
        	}

        	else {
        		box.graphics.setStrokeStyle(4).beginStroke('white').beginFill(zettelFarben[randomBit]).drawRoundRect(0, 0, boxwidth, boxheight, 6, 6);
            	container[i].addChild(box);
            }
        }

        // Den Spielfiguren (Container 1-4) Eventlistener hinzufügen
        if(i > 0) {
        	container[i].addEventListener("pressmove", handlePressMoveStart);
        	container[i].addEventListener("pressup", function(event) {
            	for (var i = 1; i < 5; i++) {
                	container[i].addEventListener("pressmove", handlePressMoveStart);
                	auswahlboxen[i-1].alpha = 0.2;
           		}
        	});
		}
        stage.addChild(container[i]);
	}
}

// Bewegt die Auswahlfelder und startet das Spiel, wenn ein Auswahlfeld in die Auswahlbox gezogen wurde
function handlePressMoveStart(event) {
    event.currentTarget.x = event.stageX;
    event.currentTarget.y = event.stageY;
    offsetX = boxwidth / 2;
    offsetY = boxheight + abstand / 2;

    if (event.currentTarget == container[1]) {
        offsetY = boxheight / 2;
        spieleranzahl = 1;
        auswahlboxen[0].alpha = 1.0;

        // Verhindern, dass mehrere Spielfiguren gleichzeitig gezogen werden können
        for (var i = 2; i < 5; i++) {
            container[i].removeEventListener("pressmove", handlePressMoveStart);
        }

    } else if (event.currentTarget == container[2]) {
        spieleranzahl = 2;

        for (var i = 0; i < 2; i++) {
            auswahlboxen[i].alpha = 1.0;
        }

        container[1].removeEventListener("pressmove", handlePressMoveStart);
        container[3].removeEventListener("pressmove", handlePressMoveStart);
        container[4].removeEventListener("pressmove", handlePressMoveStart);

    } else if (event.currentTarget == container[3]) {
        offsetX = boxwidth + abstand / 2;
        spieleranzahl = 3;

        for (var i = 0; i < 3; i++) {
            auswahlboxen[i].alpha = 1.0;
        }

        container[1].removeEventListener("pressmove", handlePressMoveStart);
        container[2].removeEventListener("pressmove", handlePressMoveStart);
        container[4].removeEventListener("pressmove", handlePressMoveStart);

    } else {
        offsetX = boxwidth + abstand / 2;
        spieleranzahl = 4;

        for (var i = 0; i < 4; i++) {
            auswahlboxen[i].alpha = 1.0;
        }

        for (var i = 1; i < 4; i++) {
            container[i].removeEventListener("pressmove", handlePressMoveStart);
        }
    }


    // Automatisch in Auswahlbox ziehen und Spiel starten
    if (event.currentTarget.x > stage.canvas.width / 2 - boxwidth - abstand / 2 - offsetX && event.currentTarget.x < stage.canvas.width / 2 - boxwidth - abstand / 2 + boxwidth * 2 + abstand + offsetX && event.currentTarget.y < (1 / 4) * stage.canvas.height - boxwidth - abstand / 2 + boxwidth * 2 + abstand + offsetY && event.currentTarget.y > (1 / 4) * stage.canvas.height - boxwidth - abstand / 2 - offsetY) {

        event.currentTarget.x = stage.canvas.width / 2 - boxwidth - abstand / 2 + offsetX;
        event.currentTarget.y = (1 / 4) * stage.canvas.height - boxwidth - abstand / 2 + offsetY;

        for (var i = 1; i < 5; i++) {
            container[i].removeEventListener("pressmove", handlePressMoveStart);
            container[i].removeAllEventListeners("pressup");
        }

        stage.removeChild(text3);
        text4.text = spieleranzahl;
		if(spieleranzahl ==1){
			text1.text =l("%spielbeginnSingular");
		}
        stage.addChild(text1);
        stage.addChild(text2);
        stage.addChild(text4);
        setTimeout(function() {
            splitScreen(spieleranzahl)
        }, 2000);
    }
    //stage.update();
}

function splitScreen(spieler) {
    // Speicher aller Spieler werden geloescht und die Stage wird geleert
    MEMORY1 = [];
    MEMORY2 = [];
    MEMORY3 = [];
    MEMORY4 = [];
    WHITEBOX1 = [];
    WHITEBOX2 = [];
    WHITEBOX3 = [];
    WHITEBOX4 = [];

    stage.removeAllChildren();
    stage.addChild(bg);

    // Unsichtbare Felder für geheimen Neustart werden erzeugt
    var rect1 = new createjs.Shape();
    rect1.graphics.beginFill("blue").drawRect(stage.canvas.width - 100, 0, 100, 100);
    rect1.addEventListener("pressup", function(event) {
        resetIt = 1;
        console.log(resetIt);
    });
    rect1.alpha = 0.01;

    var rect2 = new createjs.Shape();
    rect2.graphics.beginFill("red").drawRect(0, stage.canvas.height - 100, 100, 100);
    rect2.addEventListener("pressup", function(event) {
        resetIt2 = 1;
        console.log(resetIt2);
    });
    rect2.alpha = 0.01;

    stage.addChild(rect1);
    stage.addChild(rect2);

    //Alle potenitell benoetigten Trennlinien werden erzeugt, aber noch nicht der stage hinzugefügt
    var lineHorizontal = new createjs.Shape();
    var lineHorizontalCommand = lineHorizontal.graphics.beginFill("#00ff00").drawRect(0, stage.canvas.height / 2 - 4, stage.canvas.width, 8).command;

    var lineVertikal = new createjs.Shape();
    var lineVertikalCommand = lineVertikal.graphics.beginFill("#00ff00").drawRect(stage.canvas.width / 2 - 4, 0, 8, stage.canvas.height).command;

    var lineVertikal2 = new createjs.Shape();
    var lineVertikalCommand2 = lineVertikal2.graphics.beginFill("#00ff00").drawRect(stage.canvas.width / 3 - 4, 0, 8, stage.canvas.height).command;

    // Es werden entsprechend der Spieleranzahl die Spielfelder erzugt sowie die benötigten Trennlinien der Stage hinzugefuegt
    if (spieler == 1) {
        boxwidth = stage.canvas.width / 20;
        boxheight = stage.canvas.width / 20;
        abstand = stage.canvas.width / 96;

        erstelleFeld(stage.canvas.width / 2 - boxwidth * 3 - 3 * abstand, stage.canvas.height / 2 - boxheight * 3 - 3 * abstand, 5, 0, MEMORY1, WHITEBOX1, 0);
    } else if (spieler == 2) {
        boxwidth = stage.canvas.width / 22;
        boxheight = stage.canvas.width / 22;
        abstand = stage.canvas.width / 144;
        stage.addChild(lineVertikal);
        schriftGroesse = 20;

        erstelleFeld(stage.canvas.width / 2 - boxwidth * 3 - 3 * abstand - stage.canvas.width / 5, stage.canvas.height / 2 - boxheight * 3 - 3 * abstand, 5, 90, MEMORY1, WHITEBOX1, 0);
        erstelleFeld(stage.canvas.width / 2 - boxwidth * 2 - 2 * abstand + stage.canvas.width / 5, stage.canvas.height / 2 - boxheight * 2 - 2 * abstand, 5, 270, MEMORY2, WHITEBOX2, 1);
    } else if (spieler == 3) {
        boxheight = stage.canvas.width / 32;
        boxwidth = stage.canvas.width / 32;
        lineHorizontalCommand.w = stage.canvas.width / 2;
        lineVertikalCommand.x = 2 * (stage.canvas.width / 3) - 4;
        stage.addChild(lineVertikal);
        stage.addChild(lineVertikal2);
        schriftGroesse = 15;

        erstelleFeld(stage.canvas.width / 3 + ((stage.canvas.width / 3) - 4) / 2 - 3 * (boxwidth + abstand) - abstand, stage.canvas.height / 2 - boxheight * 3 - 4 * abstand - stage.canvas.height / 5, 5, 0, MEMORY1, WHITEBOX1, 0);
        erstelleFeld(stage.canvas.width / 6 - (1.5 * boxheight + 2 * abstand), stage.canvas.height / 2 - boxheight * 3 - 4 * abstand, 5, 90, MEMORY2, WHITEBOX2, 1);
        erstelleFeld(2 * (stage.canvas.width / 3) + (5 * boxheight + 3 * abstand) / 4, stage.canvas.height / 2 - boxheight * 2 - 2 * abstand, 5, 270, MEMORY3, WHITEBOX3, 2);
    } else {
        boxheight = stage.canvas.width / 36;
        boxwidth = stage.canvas.width / 36;
        stage.addChild(lineHorizontal);
        stage.addChild(lineVertikal);
        schriftGroesse = 15;

        erstelleFeld(stage.canvas.width / 2 - boxwidth * 2 - 2 * abstand - stage.canvas.width / 4 - stage.canvas.width / 21, stage.canvas.height / 2 - boxheight * 2 - 2 * abstand - stage.canvas.height / 4, 5, 180, MEMORY1, WHITEBOX1, 0);
        erstelleFeld(stage.canvas.width / 2 - boxwidth * 2 - 2 * abstand + stage.canvas.width / 4 - stage.canvas.width / 21, stage.canvas.height / 2 - boxheight * 2 - 2 * abstand - stage.canvas.height / 4, 5, 180, MEMORY2, WHITEBOX2, 1);
        erstelleFeld(stage.canvas.width / 2 - boxwidth * 3 - 4 * abstand - stage.canvas.width / 4 + stage.canvas.width / 21, stage.canvas.height / 2 - boxheight * 3 - 4 * abstand + stage.canvas.height / 4, 5, 0, MEMORY3, WHITEBOX3, 2);
        erstelleFeld(stage.canvas.width / 2 - boxwidth * 3 - 4 * abstand + stage.canvas.width / 4 + stage.canvas.width / 21, stage.canvas.height / 2 - boxheight * 3 - 4 * abstand + stage.canvas.height / 4, 5, 0, MEMORY4, WHITEBOX4, 3);
    }
}

function erstelleFeld(startX, startY, dim, orient, arr, arr2, farbSchema) {

    console.log(modus);
    // Spalten und anzahlFelder initialisieren
    var spalten = dim;
    var anzahlFelder = spalten * spalten;

    // Container für die ggf. nötige rotierung
    var contain = new createjs.Container();
    var id;
    var spielerText = new createjs.Text("", "14px Arial Black", "navy");

    // Variablen Einfuehrung Fehlersuche
    //var fehlerTut = [];
    var zeile, spalte;


    //Endscreen
    if (modus == 4) {
        stage.removeAllChildren();
        endscreen();

    } else {
        if (modus == 0) {
			geradeUngerade(startX, startY, dim, orient, arr, arr2, farbSchema);

        } else {

            if (arr === MEMORY1) { //Anhand des Arrays möglich
                var SpielerName = "Spieler 1";
                var anzahlFehler = fehlerP1;

            } else if (arr === MEMORY2) {
                var SpielerName = "Spieler 2";
                var anzahlFehler = fehlerP2;
            } else if (arr === MEMORY3) {
                var SpielerName = "Spieler 3";
                var anzahlFehler = fehlerP3;
            } else if (arr === MEMORY4) {
                var SpielerName = "Spieler 4";
                var anzahlFehler = fehlerP4;
            }

            for (var i = 0; i < anzahlFelder; i++) {

                box = new createjs.Shape();
                contain.addChild(box);
                id = i;
                box.id = id;
                box.name = "true";
                randomBit = Math.floor(Math.random() * 2);

                //Verschiedene Farbsets erlauben
                if (farbSchema == 0) { //Default
                    box.color = zettelFarben[randomBit];
                    shiftFarbe = 0; // Verschiebung im FarbArray
                } else if (farbSchema == 1) {
                    box.color = zettelFarben[randomBit + 2];
                    shiftFarbe = 2;
                } else if (farbSchema == 2) {
                    box.color = zettelFarben[randomBit + 4];
                    shiftFarbe = 4;
                } else if (farbSchema == 3) {
                    box.color = zettelFarben[randomBit + 6];
                    shiftFarbe = 6;

                }

                //Pushe pro Zeile erzeugte Felder ins Array
                arr.push(box);
                box.graphics.setStrokeStyle(2).beginFill(box.color).drawRoundRect(startX, startY, boxwidth, boxheight, 6, 6).endFill();
                if (orient == 0) {
                    box.shadow = new createjs.Shadow("#4C4C4C", 3, 3, 8);
                } else if (orient == 90) {
                    box.shadow = new createjs.Shadow("#4C4C4C", -3, 3, 8);
                } else if (orient == 180) {
                    box.shadow = new createjs.Shadow("#4C4C4C", -3, -3, 8);
                } else if (orient == 270) {
                    box.shadow = new createjs.Shadow("#4C4C4C", 3, -3, 8);
                }
                //Ende der Zeile erreicht? Fange wieder vorne an boxen zu erzeugen
                box.x = (boxwidth + abstand) * (i % spalten);
                box.y = (boxheight + abstand) * (i / spalten | 0);
                box.zeile = i / spalten | 0;
                box.spalte = i % spalten;



                if (modus == 3) {
                    fehlersuche(startX, startY, dim, orient, arr, arr2, farbSchema,contain);

                }




            }

            //Container für die Rotierung optimieren

            contain.x = startX + dim * (boxwidth + abstand) / 2;
            contain.y = startY + dim * (boxwidth + abstand) / 2;
            contain.regX = startX + dim * (boxwidth + abstand) / 2;
            contain.regY = startY + dim * (boxheight + abstand) / 2;
            contain.rotation = orient;



            berechneAssistent(startX, startY, dim, orient, arr, arr2, farbSchema, contain);

            if (modus == 1) {
                stapelZettel(startX, startY, dim, contain, arr, arr2, SpielerName, spielerText, orient);

                for (var j = 5; j < 25; j++) {
                    arr[j].visible = false;
                }
            } else if (modus == 3) {
                //(1) fehlerEinbauen(): markiert die Fehlerstelle durch: arr[x].name = 'false' ("kuenstiches Boolean")
                fehlerEinbauen(dim, arr, SpielerName, spieleranzahl);

                //(2) Wir suchen nun die stelle wo arr[x].name == 'false' und invertieren die Farbe
                for (var k = 0; k < (dim + 1) * (dim + 1); k++) {

                    if (arr[k].name === "false") {
                        switch (arr[k].color) {
                            case zettelFarben[0 + shiftFarbe]:
                                arr[k].graphics.beginFill(zettelFarben[0 + shiftFarbe + 1]).drawRoundRect(startX, startY, boxwidth, boxheight, 6, 6).endFill();
                                break;
                            case zettelFarben[0 + shiftFarbe + 1]:
                                arr[k].graphics.beginFill(zettelFarben[0 + shiftFarbe]).drawRoundRect(startX, startY, boxwidth, boxheight, 6, 6).endFill();
                                break;
                        }

                    }

                }



            } else if (modus == 2) {
                //stapelZettel(startX, startY, dim, contain, arr, arr2, SpielerName, spielerText, orient);

                for (var i = 0; i < dim * dim + 2 * dim + 1; i++) {
                    arr[i].alpha = 0.2;

                }
                //(1) fehlerEinbauen(): markiert die Fehlerstelle durch: arr[x].name = 'false' ("kuenstiches Boolean")
                fehlerEinbauen(dim, arr, SpielerName, spieleranzahl);

                //(2) Wir suchen nun die stelle wo arr[x].name == 'false' und invertieren die Farbe
                for (var k = 0; k < (dim + 1) * (dim + 1); k++) {

                    if (arr[k].name === "false") {
                        switch (arr[k].color) {
                            case zettelFarben[0 + shiftFarbe]:
                                arr[k].graphics.beginFill(zettelFarben[0 + shiftFarbe + 1]).drawRoundRect(startX, startY, boxwidth, boxheight, 6, 6).endFill();
                                break;
                            case zettelFarben[0 + shiftFarbe + 1]:
                                arr[k].graphics.beginFill(zettelFarben[0 + shiftFarbe]).drawRoundRect(startX, startY, boxwidth, boxheight, 6, 6).endFill();
                                break;
                        }
                        var fehlerstelle = k;
                    }
                }
                arr[fehlerstelle].graphics.beginFill(arr[fehlerstelle.color]).drawRoundRect(startX, startY, boxwidth, boxheight, 6, 6).endFill();
                arr[fehlerstelle].alpha = 0.09;
                gameTutorial(startX, startY, dim, contain, arr, arr2, SpielerName, spielerText, orient);

            }

            eule(arr, contain, startX, startY, anzahlFehler, spielerText, SpielerName);
            if (anzahlFehler == 0 && modus == 3) {
                var herzP1 = new createjs.Bitmap("img/heart3.png");
                var eulenabstand = 150 + stage.canvas.width / 34;
                if (arr == MEMORY1 && spieleranzahl == 3) {
                    herzP1.x = startX - 10 + 3 * (boxwidth + abstand) + abstand - 75 - stage.canvas.width / 21;
                    herzP1.y = startY + 8.5 * boxheight;
                } else {
                    //herzP1.x = startX-(eulenabstand+10);
                    herzP1.x = startX - eulenabstand;
                    herzP1.y = startY + 0.5 * boxheight;
                }
                contain.addChild(herzP1);

            }
        }
    }

    stage.addChild(contain);


    //stage.update();


}

function neuesFeld(arr, contain, startX, startY, anzahlFehler, spielerText, spielerName) {
    //Bei einem Spieler wird das Spiel über die Methode Splitscreen neu gestartet; bei mehr Spielern bekommt der übergebene Spieler ein neues Feld erstellt
    if (spieleranzahl == 1) {
        contain.removeAllChildren(arr);
        splitScreen(1);
    } else if (spieleranzahl == 2) {
        contain.removeAllChildren(arr);
        if (spielerName == "Spieler 1") {
            MEMORY1 = [];
            arr = MEMORY1;
            erstelleFeld(startX, startY, 5, 90, arr, WHITEBOX1, 0);
        } else if (spielerName == "Spieler 2") {
            MEMORY2 = [];
            arr = MEMORY2;
            erstelleFeld(startX, startY, 5, 270, arr, WHITEBOX2, 1);
        }

    } else if (spieleranzahl == 3) {
        contain.removeAllChildren(arr);
        if (spielerName == "Spieler 1") {
            MEMORY1 = [];
            arr = MEMORY1;
            erstelleFeld(startX, startY, 5, 0, arr, WHITEBOX1, 0);
        } else if (spielerName == "Spieler 2") {
            MEMORY2 = [];
            arr = MEMORY2;
            erstelleFeld(startX, startY, 5, 90, arr, WHITEBOX2, 1);
        } else if (spielerName == "Spieler 3") {
            MEMORY3 = [];
            arr = MEMORY3;
            erstelleFeld(startX, startY, 5, 270, arr, WHITEBOX3, 2);
        }

    } else if (spieleranzahl == 4) {
        contain.removeAllChildren(arr);
        if (spielerName == "Spieler 1") {
            MEMORY1 = [];
            arr = MEMORY1;
            erstelleFeld(startX, startY, 5, 180, arr, WHITEBOX1, 0);
        } else if (spielerName == "Spieler 2") {
            MEMORY2 = [];
            arr = MEMORY2;
            erstelleFeld(startX, startY, 5, 180, arr, WHITEBOX2, 1);
        } else if (spielerName == "Spieler 3") {
            MEMORY3 = [];
            arr = MEMORY3;
            erstelleFeld(startX, startY, 5, 0, arr, WHITEBOX3, 2);
        } else if (spielerName == "Spieler 4") {
            MEMORY4 = [];
            arr = MEMORY4;
            erstelleFeld(startX, startY, 5, 0, arr, WHITEBOX4, 3);
        }

    }
}

function eule(arr, contain, startX, startY, anzahlFehler, spielerText, SpielerName) {


    if (spieleranzahl == 4) {
        var eulenabstand = 150 + stage.canvas.width / 34;
        spielerText.x = startX - eulenabstand + 10;
        spielerText.y = startY + 2 * boxheight + 12;
    } else if (spieleranzahl == 1) {
        var eulenabstand = 150 + stage.canvas.width / 34;
        spielerText.x = startX - eulenabstand + 10;
        spielerText.y = startY + 2 * boxheight + 12;
    } else if (spieleranzahl == 2) {
        var eulenabstand = 150 + stage.canvas.width / 34;
        spielerText.x = startX - eulenabstand + 10;
        spielerText.y = startY + 2 * boxheight + 12;
    } else if (spieleranzahl == 3 && arr != MEMORY1) {
        var eulenabstand = 150 + stage.canvas.width / 34;
        spielerText.x = startX - eulenabstand + 10;
        spielerText.y = startY + 2 * boxheight + 12;
    } else if (spieleranzahl == 3 && arr == MEMORY1) {
        spielerText.x = startX - 10 + 3 * (boxwidth + abstand) + abstand - 75 - stage.canvas.width / 21 + 17;
        spielerText.y = startY + 10 * boxheight + 12;
    }
    spielerText.lineWidth = 125;
    spielerText.lineHeight = 16;
    spielerText.textBaseline = "top";
    spielerText.textAlign = "left";

    if(arr == MEMORY1) {
    	i = 0;
    } else if (arr == MEMORY2) {
    	i = 1;
    } else if (arr == MEMORY3) {
    	i = 2;
    } else if (arr == MEMORY4) {
    	i = 3;
    }

    aufgabentext[i] = new createjs.Text("", schriftGroesse + "px Arial Black", "white");
    aufgabentext[i].x = startX;
    aufgabentext[i].y = startY - (boxwidth + 1.5*abstand);

    if (modus == 1) {
        aufgabentext[i].text = l("%aufgabeModus1");
        spielerText.text = l("%euleTippModus1");
    } else if (modus == 3) {
        aufgabentext[i].text = l("%aufgabeModus3");
        spielerText.text = l("%euleTippModus3");
    } else if (modus == 0) {
        aufgabentext[i].text = l("%aufgabeModus0");
        spielerText.text = l("%euleTippModus0");
    } else if (modus == 2) {
        aufgabentext[i].text = l("%aufgabeModus2");
        spielerText.text = l("%euleTippModus2");
    }






    if (anzahlFehler > 0) {
        spielerText.text = l("%euleFalsch");

        if (anzahlFehler == 3) {
            anzahlFehler = 0;
            spielerText.text = l("%euleDreiFalsch");
            for (var i = 0; i < 36; i++) {
                arr[i].removeAllEventListeners();
                arr[i].alpha = 0.2;
                if (arr[i].name == "false") {
                    arr[i].alpha = 1.0;
                }
            }
            //3-4 Sekunden Delay

            setTimeout(function() {
                neuesFeld(arr, contain, startX, startY, anzahlFehler, spielerText, SpielerName)
            }, 2000)
        }

        return 0;

    }



    var eulenabstand = 150 + stage.canvas.width / 34;
    // SpriteSheet
    var ss = new createjs.SpriteSheet({
        "animations": {
            "run": {
                frames: [0, 1],
                speed: 2.01

            }
        },
        "images": ["img/owl_sprite.png"],
        "frames": {
            //Höhe und Breite eines Frames
            "height": 172,
            "width": 120,
        }
    });

    // Erstelle Sprite
    var sprite1 = new createjs.Sprite(ss, "run");
    sprite1.x = startX - eulenabstand;
    sprite1.y = startY + 2 * boxheight + 80;
    sprite1.addEventListener("pressup", function(event) {
                spielerText.text = "Hey, das kitzelt! :)";
            });
    contain.addChild(sprite1);

    var speechBubble = new createjs.Shape();
    speechBubble.graphics.beginFill("#f9ed14").drawRoundRect(10, 10, 150, 80, 10, 10);
    speechBubble.x = startX - (eulenabstand + 10);
    speechBubble.y = startY + 2 * boxheight;
    contain.addChild(speechBubble);
    speechBubble.visible = true;



    var bubbleSpitze = new createjs.Graphics;
    var Point = createjs.Point;

    var a = new Point(-50, 30);
    var b = new Point(-30, 100);
    var c = new Point(-80, 30);
    var radius = 50.0;

    bubbleSpitze
        .beginFill("#f9ed14")
        .moveTo(a.x, a.y)
        .arcTo(b.x, b.y, c.x, c.y, 0)
        .lineTo(c.x, c.y)
        .endStroke();

    var spitze = new createjs.Shape(bubbleSpitze);
    spitze.x = startX - (eulenabstand - 80);
    spitze.y = startY + 2 * boxheight + 20;
    spitze.visible = true;
    contain.addChild(spitze);
    contain.addChild(spielerText);
    contain.addChild(aufgabentext[i]);

    // 3 Spieler Fix
    if (spieleranzahl == 3 && arr === MEMORY1) {
        sprite1.x = startX + 3 * (boxwidth + abstand) + abstand - 75 - stage.canvas.width / 21;
        sprite1.y = startY + 10 * boxheight + 80;
        speechBubble.x = startX - 10 + 3 * (boxwidth + abstand) + abstand - 75 - stage.canvas.width / 21;
        speechBubble.y = startY + 10 * boxheight;

        spitze.x = startX + 80 + 3 * (boxwidth + abstand) + abstand - 75 - stage.canvas.width / 21;
        spitze.y = startY + 10 * boxheight + 20;
    }
    console.log(aufgabentext[i].text);

}

function berechneAssistent(startX, startY, dim, orient, arr, arr2, farbSchema, container) {
    //Positionsvariablen Boxen
    var pruefPositionX = startX + dim * (boxwidth + abstand) + abstand;
    var pruefPositionY = 0;
    var pruefFarbe; //Farbe der Box wird hier festgehalten
    var startIndex = dim * dim; //ID der jeweiligen Boxen

    //verschiedenste Zaehlvariablen
    var j = 0;
    var u = 0;
    var zaehlAssistent = 0;
    var blau;
    var rot;

    //Alle Assistensfelder werden nacheinander erzeugt
    for (var i = 0; i < dim + dim + 1; i++) {
        startIndex += i;
        blau = 0;
        rot = 0;
        var id = startIndex;
        var box = new createjs.Shape();
        container.addChild(box);
        box.id = id;

        if (orient == 0) {
            box.shadow = new createjs.Shadow("#4C4C4C", 3, 3, 8);
        } else if (orient == 90) {
            box.shadow = new createjs.Shadow("#4C4C4C", -3, 3, 8);
        } else if (orient == 180) {
            box.shadow = new createjs.Shadow("#4C4C4C", -3, -3, 8);
        } else if (orient == 270) {
            box.shadow = new createjs.Shadow("#4C4C4C", 3, -3, 8);
        }

        if (zaehlAssistent < dim) { //Farbzettel der Zeile werden gezaehlt und die Position des Assistenten bestimmt

            for (var k = 0 + j; k < dim + j; k++) {
                if (arr[k].color == zettelFarben[0 + shiftFarbe]) {
                    rot += 1;
                } else {
                    blau += 1;
                }
            }
            box.y = pruefPositionY;
            pruefPositionY += boxheight + abstand; //y-Koordinate wird für nächstes Feld verschoben
            j += dim; //Zaehlvariable wird auf naechste Zeile verschoben

        } else if (zaehlAssistent < dim + dim) { //Farbzettel der Spalte werden gezaehlt und die Position des Assistenten bestimmt

            if (zaehlAssistent == dim) {
                pruefPositionY = startY + dim * (boxheight + abstand) + abstand;
                pruefPositionX = 0;
            }

            for (var k = 0 + u; k < dim * dim; k += dim) {
                if (arr[k].color == zettelFarben[0 + shiftFarbe]) {
                    rot += 1;
                } else if (k % dim == 0) {
                    blau += 1;
                }
            }

            box.x = pruefPositionX;
            pruefPositionX += boxwidth + abstand;
            u += 1;

        } else { // Farbe fuer die Ecke und die Position des Assistenten wird bestimmt
            var last = dim * dim + dim;
            for (var k = last; k < last + dim; k++) {
                if (arr[k].color == zettelFarben[0 + shiftFarbe]) {
                    rot += 1;
                } else {
                    blau += 1;
                }
            }
            box.x = pruefPositionX + abstand;
        }
        //Farbe wird der Zaehlung entsprechend festgelegt
        if (rot % 2 != 0) {
            pruefFarbe = zettelFarben[0 + shiftFarbe];
        } else {
            pruefFarbe = zettelFarben[1 + shiftFarbe];
        }
        //Box wird dem Modus entsprechend erzeugt
        if (modus == 1) {
            if (zaehlAssistent < dim) {
                box.graphics.setStrokeStyle(4).beginStroke('#4C4C4C').beginFill("white").drawRoundRect(pruefPositionX, startY, boxwidth, boxheight, 6, 6).endFill();
            } else {
                box.graphics.setStrokeStyle(4).beginStroke('#4C4C4C').beginFill("white").drawRoundRect(startX, pruefPositionY, boxwidth, boxheight, 6, 6).endFill();
            }
        } else if (modus == 3) {
            if (zaehlAssistent < dim) {
                box.graphics.setStrokeStyle(4).beginStroke("black").beginFill(pruefFarbe).drawRoundRect(pruefPositionX, startY, boxwidth, boxheight, 6, 6).endFill();
            } else {
                box.graphics.setStrokeStyle(4).beginStroke("black").beginFill(pruefFarbe).drawRoundRect(startX, pruefPositionY, boxwidth, boxheight, 6, 6).endFill();
                box.shadow = 0;
            }
        } else if (modus == 2) {

            if (zaehlAssistent < dim) {
                box.graphics.setStrokeStyle(2).beginStroke('black').beginFill(pruefFarbe).drawRoundRect(pruefPositionX, startY, boxwidth, boxheight, 6, 6).endFill();
                //box.y = pruefPositionY;
            } else {
                box.graphics.setStrokeStyle(2).beginStroke('black').beginFill(pruefFarbe).drawRoundRect(startX, pruefPositionY, boxwidth, boxheight, 6, 6).endFill();
                //box.x = pruefPositionX;
            }

        }

        box.color = pruefFarbe;
        arr.push(box);
        zaehlAssistent += 1;

        // Für die "Assistent-Ergänzen" Einführung werden alle Assistensfelder bis auf das erste unsichtbar gemacht
        if (modus == 1) {
            arr2.push(box);
            if (zaehlAssistent != 1) {
                box.visible = false;
            }
        } else if (modus == 2) {
            arr2.push(box);
        }

    }

}


function fehlerEinbauen(dim, arr, SpielerName, spieleranzahl) {

    //Zufaellige Fehlerstelle im Array "markieren"
    var fehlerBereich = (dim * dim) - 1;
    var fehlerPos = Math.floor(Math.random() * fehlerBereich) + 0;
    arr[fehlerPos].name = "false";

    if (modus != 2) {
        /*
        //Abfrage, ob das Fehler-Tutorial mit hervorgehobener Zeile und Spalte schon gespielt wurde
        var enthalten = "false";
        for (var f = 0; f < spieleranzahl; f++) {
            if (fehlerTut[f] == SpielerName) {
                enthalten = "true";
            }
        }
        // Turtorial mit hervorgehobener Zeile und Spalte wird gestartet, wenn noch es noch nicht gespielt wurde
        if (enthalten != "true") {
            for (var i = 0; i < dim * dim; i++) {
                if (arr[i].zeile != arr[fehlerPos].zeile && arr[i].spalte != arr[fehlerPos].spalte) {
                    arr[i].alpha = 0.2;
                    arr[i].removeAllEventListeners("pressup");
                }
            }
        }
        */
    }


    //stage.update();


}

function ranking(startX, startY, arr, contain, SpielerName, spielerText) {

    //Die Variablen für die Rückmeldung zum Rang, sowie der Feedbacktext der Eule werden erzeugt und der Stage hinzugefügt
    var rankingText = new createjs.Text("", "17px Arial black", "gold");
    rankingText.x = startX + boxheight * 2 - 3 * abstand;
    rankingText.y = startY - 30;
    contain.addChild(rankingText);

    spielerText.text = l("%euleRichtig");
    contain.addChild(spielerText);

    if(arr == MEMORY1) {
    	i = 0;
    } else if (arr == MEMORY2) {
    	i = 1;
    } else if (arr == MEMORY3) {
    	i = 2;
    } else if (arr == MEMORY4) {
    	i = 3;
    }

    console.log(i);
    console.log(aufgabentext[i].text);

    //Entsprechend des Ranges wird dem Spieler Rückmeldung geegeben und der Spielername dem Array zur SPeicherung dessen übergeben
    if (spieleranzahl > 1) {
        if (rankingCount == 0) {
            rankingText.text = l("%erster");
            aufgabentext[i].text = l("%aufgabeWarten");
            RANK[0] = SpielerName;

        } else if (rankingCount == 1) {
            rankingText.text = l("%zweiter");
            aufgabentext[i].text = l("%aufgabeWarten");
            RANK[1] = SpielerName;

        } else if (rankingCount == 2) {
            rankingText.text = l("%dritter");
            aufgabentext[i].text = l("%aufgabeWarten");
            RANK[2] = SpielerName;

        } else {
            rankingText.text = l("%vierter");
            aufgabentext[i].text = l("%aufgabeWarten");
            RANK[3] = SpielerName;

        }
    }


    alleFertig += 1;
    rankingCount += 1;
}
