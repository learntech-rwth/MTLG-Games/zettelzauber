function stapelZettel(startX, startY, dim, contain, arr, arr2, SpielerName, spielerText, orient) {

    // Besen zum Aufräumen der Zettel
    besen = new createjs.Bitmap("img/besen.png");

    if (spieleranzahl == 3 && arr === MEMORY1) {
        besen.x = startX + 6 * (boxwidth + abstand) + abstand - stage.canvas.width / 34 - stage.canvas.width / 21;
        besen.y = startY + 10 * boxheight + 100;
    } else {
        besen.x = startX + 6 * (boxwidth + abstand) + stage.canvas.width / 34;
        besen.y = startY + 2 * boxheight + 100;
    }

    besen.addEventListener("pressup", function(event) {
        for (var i = 0; i < 22; i++) {
            if (zettelstapel[i].name == "false") {
                if (zettelstapel[i].id < 11) {
                    zettelstapel[i].x = startX + offsetX + abstand + boxwidth - zettelstapel[i].id * 2;
                    zettelstapel[i].y = startY + 6 * (boxheight + abstand) + 30 + offsetY - zettelstapel[i].id * 2;
                } else {
                    zettelstapel[i].x = startX + offsetX + 4 * (abstand + boxwidth) - (zettelstapel[i].id - 11) * 2;
                    zettelstapel[i].y = startY + 6 * (boxheight + abstand) + 30 + offsetY - (zettelstapel[i].id - 11) * 2;
                }
            }
        }
    });
    contain.addChild(besen);

    // Erste Zeile sichtbar machen
    for (var j = 5; j < 25; j++) {
        arr[j].visible = false;
    }
    
    // Erzeugung der Zettelstapel
    var zettelstapel = [];
    var zettel;
    var tempX = startX + dim*(boxwidth) + (dim+1)*abstand;
    var tempY = startY;
    var count = 0;
    offsetX = boxwidth / 2;
    offsetY = boxheight / 2;

    for (var i = 0; i < 22; i++) {
        zettel = new createjs.Shape();

        // Schattenkorrektur
        if (orient == 0) {
            zettel.shadow = new createjs.Shadow("#4C4C4C", 3, 3, 8);
        } else if (orient == 90) {
            zettel.shadow = new createjs.Shadow("#4C4C4C", -3, 3, 8);
        } else if (orient == 180) {
            zettel.shadow = new createjs.Shadow("#4C4C4C", -3, -3, 8);
        } else if (orient == 270) {
            zettel.shadow = new createjs.Shadow("#4C4C4C", 3, -3, 8);
        }

        zettel.regX = boxwidth / 2;
        zettel.regY = boxheight / 2;
        zettel.id = i;
        zettel.name = "false";

        if (i < 11) {
            zettel.graphics.setStrokeStyle(4).beginStroke('white').beginFill(zettelFarben[shiftFarbe]).drawRoundRect(0, 0, boxwidth, boxheight, 6, 6);
            zettel.color = zettelFarben[shiftFarbe];
            zettel.x = startX + offsetX + 1 * (abstand + boxwidth) - i * 2;
            zettel.y = startY + 6 * (boxheight + abstand) + 30 + offsetY - i * 2;
        } else {
            zettel.graphics.setStrokeStyle(4).beginStroke('white').beginFill(zettelFarben[shiftFarbe + 1]).drawRoundRect(0, 0, boxwidth, boxheight, 6, 6);
            zettel.color = zettelFarben[shiftFarbe + 1];
            zettel.x = startX + offsetX + 4 * (abstand + boxwidth) - (i - 11) * 2;
            zettel.y = startY + 6 * (boxheight + abstand) + 30 + offsetY - (i - 11) * 2;
        }

        zettel.addEventListener("pressmove", function(event) {
            timeIn = (new Date()).getTime() + timeOut;

            // Fix für Pressmove 
            if (spieleranzahl == 4 && arr === MEMORY2) {
                event.currentTarget.x = -event.stageX + (3 / 2) * demoCanvas.width + (boxwidth + abstand) - (stage.canvas.width / 21) * 2;
                event.currentTarget.y = -event.stageY + demoCanvas.height / 2 + (boxheight + abstand);
            } else if (spieleranzahl == 4 && arr === MEMORY1) {
                event.currentTarget.x = -event.stageX + (1 / 2) * demoCanvas.width + (boxwidth + abstand) - (stage.canvas.width / 21) * 2;
                event.currentTarget.y = -event.stageY + demoCanvas.height / 2 + (boxheight + abstand);
            } else if (spieleranzahl == 2 && arr === MEMORY1) {
                event.currentTarget.y = -event.stageX + stage.canvas.width / 2 + offsetX + abstand;
                event.currentTarget.x = event.stageY + offsetY;
            } else if (spieleranzahl == 2 && arr === MEMORY2) {
                event.currentTarget.y = event.stageX - stage.canvas.width / 2 + offsetX + boxwidth + 2 * abstand;
                event.currentTarget.x = -event.stageY + demoCanvas.width + offsetY + abstand;
            } else if (spieleranzahl == 3 && arr === MEMORY2) { // 3 Spieler Links
                event.currentTarget.y = -event.stageX + stage.canvas.width / 3 + offsetX + 3.5 * boxheight;
                event.currentTarget.x = event.stageY - offsetY - 1.5 * boxheight + abstand;
            } else if (spieleranzahl == 3 && arr === MEMORY3) { // 3 Spieler Rechts
                event.currentTarget.y = event.stageX - stage.canvas.width / 3 - offsetX - 4.5 * boxheight - 2 * abstand;
                event.currentTarget.x = -event.stageY + demoCanvas.width + offsetY + 2.5 * boxheight + abstand;
            } else {
                event.currentTarget.x = event.stageX;
                event.currentTarget.y = event.stageY;
            }

            // Grenzen ueberschreiten verhindern für 4 Spieler
            if (spieleranzahl == 4) {
                if (arr == MEMORY3 || arr == MEMORY4) {
                    if (arr == MEMORY3 && event.stageX >= stage.canvas.width / 2 - 8 - offsetX && event.stageY <= stage.canvas.height / 2 + 8 + offsetY) {
                        event.currentTarget.x = stage.canvas.width / 2 - 8 - offsetX;
                        event.currentTarget.y = stage.canvas.height / 2 + 8 + offsetY;
                    } else if (arr == MEMORY3 && event.stageX >= stage.canvas.width / 2 - 8 - offsetX) {
                        event.currentTarget.x = stage.canvas.width / 2 - 8 - offsetX;
                    } else if (arr == MEMORY4 && event.stageX <= stage.canvas.width / 2 + 8 + offsetX && event.stageY <= stage.canvas.height / 2 + 8 + offsetY) {
                        event.currentTarget.x = stage.canvas.width / 2 + 8 + offsetX;
                        event.currentTarget.y = stage.canvas.height / 2 + 8 + offsetY;
                    } else if (arr == MEMORY4 && event.stageX <= stage.canvas.width / 2 + 8 + offsetX) {
                        event.currentTarget.x = stage.canvas.width / 2 + 8 + offsetX;
                    } else if (event.stageY <= stage.canvas.height / 2 + 8 + offsetY) {
                        event.currentTarget.y = stage.canvas.height / 2 + 8 + offsetY;
                    }
                } else if (arr == MEMORY2 || arr == MEMORY1) {
                    if (arr == MEMORY2 && event.stageX <= stage.canvas.width / 2 + 8 + offsetX && event.stageY >= stage.canvas.height / 2 - 8 - offsetY) {
                        event.currentTarget.x = stage.canvas.width / 2 + 8 + offsetX + stage.canvas.width / 2.49;
                        event.currentTarget.y = stage.canvas.height / 2 - 8 - offsetY - stage.canvas.height / 2.65;
                    } else if (arr == MEMORY2 && event.stageX <= stage.canvas.width / 2 + 8 + offsetX) {
                        event.currentTarget.x = stage.canvas.width / 2 + 8 + offsetX + stage.canvas.width / 2.49;
                    } else if (arr == MEMORY1 && event.stageX >= stage.canvas.width / 2 - 8 - offsetX && event.stageY >= stage.canvas.height / 2 - 8 - offsetY) {
                        event.currentTarget.x = stage.canvas.width / 2 - 8 - offsetX - stage.canvas.width / 1.9;
                        event.currentTarget.y = stage.canvas.height / 2 - 8 - offsetY - stage.canvas.height / 2.65;
                    } else if (arr == MEMORY1 && event.stageX >= stage.canvas.width / 2 - 8 - offsetX) {
                        event.currentTarget.x = stage.canvas.width / 2 - 8 - offsetX - stage.canvas.width / 1.9;
                    } else if (event.stageY >= stage.canvas.height / 2 - 8 - offsetY) {
                        event.currentTarget.y = stage.canvas.height / 2 - 8 - offsetY - stage.canvas.height / 2.65;
                    }
                }

            // Grenzen ueberschreiten verhindern für 3 Spieler
            } else if (spieleranzahl == 3) {
                if (arr == MEMORY1) {
                    if (event.stageX <= stage.canvas.width / 3 + 8 + offsetX) {
                        event.currentTarget.x = stage.canvas.width / 3 + 8 + offsetX;
                    } else if (event.stageX >= 2 * (stage.canvas.width / 3) - 8 - offsetX) {
                        event.currentTarget.x = 2 * (stage.canvas.width / 3) - 8 - offsetX;

                    }
                } else if (arr == MEMORY2) {
                    if (event.stageX >= stage.canvas.width / 3 - 8 - offsetX) {
                        event.currentTarget.y = stage.canvas.width / 3 - 8 - offsetX - stage.canvas.width / 5.9;
                    }
                } else if (arr == MEMORY3) {
                    if (event.stageX <= 2 * (stage.canvas.width / 3) + 8 + offsetX) {
                        event.currentTarget.y = stage.canvas.width / 3 + 8 + offsetX - stage.canvas.width / 5.9;
                    }
                }

            // Grenzen ueberschreiten verhindern für 2 Spieler
            } else if (spieleranzahl == 2) {
                if (arr == MEMORY1) {
                    if (event.stageX >= stage.canvas.width / 2 - 8 - offsetX) {
                        event.currentTarget.y = stage.canvas.width / 2 - 8 - offsetX - 8 * (boxheight + abstand);
                    }
                } else if (arr == MEMORY2) {
                    if (event.stageX <= stage.canvas.width / 2 + 8 + offsetX) {
                        event.currentTarget.y = stage.canvas.width / 2 + 8 + offsetX - 8 * (boxheight + abstand);
                    }
                }
            }

            // Reinziehen der Stapel in die weißen Boxen
            if (event.currentTarget.x > tempX - offsetX && event.currentTarget.x < tempX + boxwidth + offsetX && event.currentTarget.y < tempY + boxheight + offsetY && event.currentTarget.y > tempY - offsetY && event.currentTarget.color == arr[25 + count].color) {
                spielerText.text = l("%euleRichtigeFarbe");
                contain.addChild(spielerText);

                event.currentTarget.removeAllEventListeners();
                event.currentTarget.name = "true";
                event.currentTarget.x = tempX + offsetX;
                event.currentTarget.y = tempY + offsetY;
                arr2[count].visible = false;
                if(count < 10) {
                    event.currentTarget.alpha = 0.2;
                    arr2[count+1].visible = true;
                }

                // Einblenden der nächsten Zeile/Spalte und vorherige Zeile/Spalte transparent machen
                if (count < 4) {
                    for (var j = 5 + count * 5; j < 10 + count * 5; j++) {
                        arr[j].visible = true;
                        arr[j-5].alpha = 0.2;
                    }
                    tempY = tempY + boxheight + abstand;
                } else if(count == 4) {
                    for (var j = 5 + count * 5; j < 10 + count * 5; j++) {
                        arr[j-5].alpha = 0.2;
                    }
                    for (var j = 0; j < 5; j++) {
                        arr[j*5].alpha = 1;
                    }
                    tempX = startX;
                    tempY = tempY + boxheight + 2*abstand;
                } else if(count == 9) {
                    for (var j = 0; j < 5; j++) {
                        arr[j*5+(count-5)].alpha = 0.2;
                    }
                    for(var j = 0; j < 22; j++) {
                        zettelstapel[j].alpha = 1;
                    }
                    tempX = tempX + boxwidth + 2*abstand;
                } else if(count == 10) {
                    ranking(startX, startY, arr, contain, SpielerName, spielerText);
                    if (alleFertig == spieleranzahl) {
                        modus = 2;
                        alleFertig = 0;
                        rankingCount = 0;
                        setTimeout(function() {splitScreen(spieleranzahl)}, 1000);
                    }
                } else {
                    for (var j = 0; j < 5; j++) {
                        arr[j*5+(count-4)].alpha = 1;
                        arr[j*5+(count-5)].alpha = 0.2;
                    }
                    tempX = tempX + boxwidth + abstand;
                }
                
                count = count + 1;

            } else if (event.currentTarget.x > tempX - offsetX && event.currentTarget.x < tempX + boxwidth + offsetX && event.currentTarget.y < tempY + boxheight + offsetY && event.currentTarget.y > tempY - offsetY) {
                spielerText.text = l("%euleFalscheFarbe");
                contain.addChild(spielerText);

                if (event.currentTarget.id < 11) {
                    event.currentTarget.x = startX + offsetX + 1 * (abstand + boxwidth) - event.currentTarget.id * 2;
                    event.currentTarget.y = startY + 6 * (boxheight + abstand) + 30 + offsetY - event.currentTarget.id * 2;
                } else {
                    event.currentTarget.x = startX + offsetX + 4 * (abstand + boxwidth) - (event.currentTarget.id - 11) * 2;
                    event.currentTarget.y = startY + 6 * (boxheight + abstand) + 30 + offsetY - (event.currentTarget.id - 11) * 2;
                }
            }
            //stage.update();
        });

        zettelstapel[i] = zettel;
        contain.addChild(zettelstapel[i]);
    }
}

function gameTutorial(startX, startY, dim, contain, arr, arr2, SpielerName, spielerText, orient) {

    var zettelstapel = [];

    var zettel;

    for (var i = 0; i < dim; i++) {
        arr[i].shadow = new createjs.Shadow("white", 0, 0, 15);
        arr[i].alpha = 1;
        arr.last = dim;
        arr.first = 0;
        arr.nextAssistent = (dim * dim);
        arr.spaltenPos = 0;
        arr.push(arr.last, arr.first, arr.nextAssistent, arr.spaltenPos);
    }
    arr[arr.nextAssistent].shadow = new createjs.Shadow("white", 0, 0, 15);
    arr[arr.nextAssistent].alpha = 1;
	
	if (arr === MEMORY1) { //Anhand des Arrays möglich
		j =0;
	} else if (arr === MEMORY2) {
		j=1;
	} else if (arr === MEMORY3) {
		j=2;
	} else if (arr === MEMORY4) {
		j=3;
	}
	
    RICHTIGZETTEL[j] = new createjs.Bitmap("img/jaButton.png");
    FALSCHZETTEL[j] = new createjs.Bitmap("img/neinButton.png");
    RICHTIGZETTEL[j].x = startX;
    FALSCHZETTEL[j].x = startX + dim * boxwidth;
    FALSCHZETTEL[j].y = RICHTIGZETTEL[j].y = startY + (dim + 2.5) * boxheight;
	
	


    RICHTIGZETTEL[j].addEventListener("pressup", function(event) {
        var last = arr[(dim * dim) + 2 * dim + 1];
        var first = arr[(dim * dim) + 2 * dim + 2];
        var nextAssistent = arr[(dim * dim) + 2 * dim + 3];
        var k = 0;

        if (nextAssistent < dim * dim + dim) {
            for (var i = first; i < last; i++) {

                if (arr[i].name == "false") {
                    k = 1;
                }
            }
            if (k == 0) {
                correct = new createjs.Bitmap("img/correct.png");
                if (spieleranzahl == 4 || spieleranzahl == 3) {
                    correct.scaleX = correct.scaleY = 0.5;
                }

                correct.x = startX + (dim + 2.5) * boxwidth;
                correct.y = startY + arr[nextAssistent].y;
                spielerText.text = l("%euleRichtigeZeile");
                for (var i = first; i < last; i++) {
                    arr[i].shadow = new createjs.Shadow("white", 0, 0, 0);
                    arr[i].alpha = 0.4;
                }
                arr[nextAssistent].shadow = new createjs.Shadow("white", 0, 0, 0);
                arr[nextAssistent].alpha = 0.4;
                arr[(dim * dim) + 2 * dim + 3] = nextAssistent + 1;
                arr[(dim * dim) + 2 * dim + 2] = first + dim;
                arr[(dim * dim) + 2 * dim + 1] = last + dim;

                last = arr[(dim * dim) + 2 * dim + 1];
                first = arr[(dim * dim) + 2 * dim + 2];
                nextAssistent = arr[(dim * dim) + 2 * dim + 3];

                for (var i = first; i < last; i++) {
                    arr[i].shadow = new createjs.Shadow("white", 0, 0, 15);
                    arr[i].alpha = 1;
                    arr[nextAssistent].shadow = new createjs.Shadow("white", 0, 0, 15);
                    arr[nextAssistent].alpha = 1;

                }

                contain.addChild(correct);
                if (nextAssistent == 30) {
                    for (var i = 0; i < dim * dim + 2 * dim; i++) {
                        if (arr[i].spalte == arr[20].spalte) {
                            arr[i].shadow = new createjs.Shadow("white", 0, 0, 15);
                            arr[i].alpha = 1;
                        }
                    }
                    arr[(dim * dim) + 2 * dim + 4] = 20;

                    for (var i = 0; i < dim * dim; i++) {
                        if (arr[i].name == "false") {
                            var fehlerstelle = i;
                        }
                    }

                    for (var i = dim * dim; i < dim * dim + dim; i++) {
                        if (arr[i].zeile != arr[fehlerstelle].zeile) {
                            arr[i].alpha = 0.4;
                        }
                    }
                    spielerText.text = l("%euleSpaltenStart");
                }
            }
            if (k == 1) {
                spielerText.text = l("%euleFalscheZeile");
            }




        } else if (nextAssistent >= dim * dim + dim && nextAssistent < dim * dim + 2 * dim) {
            var spaltenPos = arr[(dim * dim) + 2 * dim + 4];
            var k = 0;
            var nextAssistent = arr[(dim * dim) + 2 * dim + 3];
            var fehlerPosition;
            for (var i = 0; i < dim * dim; i++) {
                if (arr[i].name == "false") {
                    fehlerPosition = i;
                }
            }


            for (var i = 0; i < dim * dim + 2 * dim; i++) {
                if (arr[i].spalte == arr[spaltenPos].spalte) {
                    if (arr[i].name == "false") {
                        k = 1;
                        fehlerPosition = i;
                    }

                }
            }


            if (k == 1) {
                //arr[i].shadow = new createjs.Shadow("white", 0, 0, 15);
                //arr[i].alpha = 1;
                //alert("test");
                spielerText.text = l("%euleFalscheSpalte");
            }
            if (k == 0) {
                spielerText.text = l("%euleRichtigeSpalte");
                correct = new createjs.Bitmap("img/correct.png");
                if (spieleranzahl == 4 || spieleranzahl == 3) {
                    correct.scaleX = correct.scaleY = 0.5;
                }

                correct.x = startX + arr[nextAssistent].x;
                correct.y = startY - boxheight+abstand;
                contain.addChild(correct);

                for (var i = 0; i < dim * dim + 2 * dim; i++) {

                    if (arr[i].spalte == arr[spaltenPos].spalte) {
                        if (arr[i].zeile != arr[fehlerPosition].zeile) {
                            arr[i].shadow = new createjs.Shadow("white", 0, 0, 0);
                            arr[i].alpha = 0.4;
                        } else {
                            arr[i].shadow = new createjs.Shadow("red", 0, 0, 0);
                        }

                    }
                }
                for (var i = 0; i < dim * dim + 2 * dim; i++) {

                    if (arr[i].spalte == arr[spaltenPos + 1].spalte) {
                        arr[i].shadow = new createjs.Shadow("white", 0, 0, 15);
                        arr[i].alpha = 1;
                        arr[nextAssistent + 1].alpha = 1;
                        arr[nextAssistent + 1].shadow = new createjs.Shadow("white", 0, 0, 15);

                    }
                }

                arr[nextAssistent].shadow = new createjs.Shadow("white", 0, 0, 0);
                arr[nextAssistent].alpha = 0.4;

                arr[(dim * dim) + 2 * dim + 4] = spaltenPos + 1;
                arr[(dim * dim) + 2 * dim + 3] = nextAssistent + 1;
            }

            nextAssistent = arr[(dim * dim) + 2 * dim + 3];

            if (nextAssistent == 35) {
                for (var i = dim * dim; i < (dim * dim) + 2 * dim + 1; i++) {

                    arr[i].alpha = 0.4;

                }

                spielerText.text = l("%euleTippErsteFehlersuche");
				
				if (arr === MEMORY1) { //Anhand des Arrays möglich
					contain.removeChild(RICHTIGZETTEL[0]);
					contain.removeChild(FALSCHZETTEL[0]);
				} else if (arr === MEMORY2) {
					contain.removeChild(RICHTIGZETTEL[1]);
					contain.removeChild(FALSCHZETTEL[1]);
				} else if (arr === MEMORY3) {
					contain.removeChild(RICHTIGZETTEL[2]);
					contain.removeChild(FALSCHZETTEL[2]);
				} else if (arr === MEMORY4) {
					contain.removeChild(RICHTIGZETTEL[3]);
					contain.removeChild(FALSCHZETTEL[3]);
				}
				
				
				if(arr == MEMORY1) {
                        i = 0;
                    } else if (arr == MEMORY2) {
                        i = 1;
                    } else if (arr == MEMORY3) {
                        i = 2;
                    } else if (arr == MEMORY4) {
                        i = 3;
                    }

                aufgabentext[i].text = l("%aufgabeErsteFehlersuche");

                arr[fehlerPosition].addEventListener("pressup", function(event) {

                    if(spieleranzahl==1){
						spielerText.text = l("%euleRichtig");
					} else{
						spielerText.text = l("%euleRichtigWarten");
					}
					
                if(arr == MEMORY1) {
                        i = 0;
                    } else if (arr == MEMORY2) {
                        i = 1;
                    } else if (arr == MEMORY3) {
                        i = 2;
                    } else if (arr == MEMORY4) {
                        i = 3;
                    }

                    aufgabentext[i].text = l("%aufgabeWarten");
                    var richtig = new createjs.Text(l("%richtig"), boxheight + "px Arial black", "white");
                    richtig.x = startX + event.target.x + boxwidth / 2 - richtig.getMeasuredWidth() / 2;
                    richtig.y = startY + event.target.y - boxwidth / 4;
                    contain.addChild(richtig);
                    alleFertig++;
                    if (alleFertig == spieleranzahl) {
                        modus = 3;
                        alleFertig = 0;
                        rankingCount = 0;
                        setTimeout(function() {splitScreen(spieleranzahl)}, 2000);
                    }
                    arr[fehlerPosition].removeAllEventListeners();

                });



            }




        }


        arr[dim * dim + 2 * dim].alpha = 0.4;
    });

    FALSCHZETTEL[j].addEventListener("pressup", function(event) {


        var last = arr[(dim * dim) + 2 * dim + 1];
        var first = arr[(dim * dim) + 2 * dim + 2];
        var nextAssistent = arr[(dim * dim) + 2 * dim + 3];
        var k = 0;

        if (nextAssistent < dim * dim + dim) {

            for (var i = first; i < last; i++) {
                //alert(i);
                if (arr[i].name == "false") {
                    k = 1;
                }
            }
            if (k == 1) {
                spielerText.text = l("%euleRichtigeZeile");
                falschZettelZeile = new createjs.Bitmap("img/fehlerzeile2.png");
                if (spieleranzahl == 4 || spieleranzahl == 3) {
                    falschZettelZeile.scaleX = falschZettelZeile.scaleY = 0.7;
                }

                falschZettelZeile.x = startX + (dim + 2.5) * boxwidth;
                falschZettelZeile.y = startY + arr[nextAssistent].y;

                for (var i = first; i < last; i++) {
                    arr[i].shadow = new createjs.Shadow("white", 0, 0, 0);
                    arr[i].alpha = 1;
                }
                //arr[nextAssistent].shadow = new createjs.Shadow("white", 0, 0, 0);
                //arr[nextAssistent].alpha = 0.4;
                arr[(dim * dim) + 2 * dim + 3] = nextAssistent + 1;
                arr[(dim * dim) + 2 * dim + 2] = first + dim;
                arr[(dim * dim) + 2 * dim + 1] = last + dim;

                last = arr[(dim * dim) + 2 * dim + 1];
                first = arr[(dim * dim) + 2 * dim + 2];
                nextAssistent = arr[(dim * dim) + 2 * dim + 3];


                for (var i = first; i < last; i++) {
                    arr[i].shadow = new createjs.Shadow("white", 0, 0, 15);
                    arr[nextAssistent].shadow = new createjs.Shadow("white", 0, 0, 15);
                    arr[nextAssistent].alpha = 1;
                    arr[i].alpha = 1;
                }

                contain.addChild(falschZettelZeile);


                if (nextAssistent == 30) {
                    for (var i = 0; i < dim * dim + 2 * dim; i++) {
                        if (arr[i].spalte == arr[20].spalte) {
                            arr[i].shadow = new createjs.Shadow("white", 0, 0, 15);
                            arr[i].alpha = 1;
                        }
                    }
                    arr[(dim * dim) + 2 * dim + 4] = 20;

                    for (var i = 0; i < dim * dim; i++) {
                        if (arr[i].name == "false") {
                            var fehlerstelle = i;
                        }
                    }

                    for (var i = dim * dim; i < dim * dim + dim; i++) {
                        if (arr[i].zeile != arr[fehlerstelle].zeile) {
                            arr[i].alpha = 0.4;
                        }
                    }

                }




            }




            if (k == 0) {
                spielerText.text = l("%euleFalscheZeile");
            }



        } else if (nextAssistent >= dim * dim + dim && nextAssistent < dim * dim + 2 * dim) {
            var spaltenPos = arr[(dim * dim) + 2 * dim + 4];
            var k = 0;
            var fehlerPosition;
            var nextAssistent = arr[(dim * dim) + 2 * dim + 3];



            for (var i = 0; i < dim * dim + 2 * dim; i++) {
                if (arr[i].spalte == arr[spaltenPos].spalte) {
                    if (arr[i].name == "false") {
                        k = 1;
                        fehlerPosition = i;
                    }

                }
            }

            if (k == 0) {
                //arr[i].shadow = new createjs.Shadow("white", 0, 0, 15);
                //arr[i].alpha = 1;
                spielerText.text = l("%euleFalscheZeile");
            }
            if (k == 1) {
                spielerText.text = l("%euleRichtigeSpalte");

                falschZettelSpalte = new createjs.Bitmap("img/fehlerspalte2.png");
                if (spieleranzahl == 4 || spieleranzahl == 3) {
                    falschZettelSpalte.scaleX = falschZettelSpalte.scaleY = 0.7;
                }

                falschZettelSpalte.x = startX + arr[nextAssistent].x;
                
                if(spieleranzahl==2 || spieleranzahl == 3){
                    falschZettelSpalte.y = startY - 2 * boxheight + 2*abstand;
                }else{
                    falschZettelSpalte.y = startY - 2 * boxheight + abstand;
                }
                contain.addChild(falschZettelSpalte);

                for (var i = 0; i < dim * dim + 2 * dim; i++) {

                    if (arr[i].spalte == arr[spaltenPos].spalte) {
                        arr[i].shadow = new createjs.Shadow("white", 0, 0, 0);
                        arr[i].alpha = 1;
                    }
                }
                for (var i = 0; i < dim * dim + 2 * dim; i++) {

                    if (arr[i].spalte == arr[spaltenPos + 1].spalte) {
                        arr[i].shadow = new createjs.Shadow("white", 0, 0, 15);
                        arr[i].alpha = 1;
                    }
                }

                arr[nextAssistent + 1].shadow = new createjs.Shadow("white", 0, 0, 0);
                arr[nextAssistent + 1].alpha = 1;

                arr[(dim * dim) + 2 * dim + 4] = spaltenPos + 1;
                arr[(dim * dim) + 2 * dim + 3] = nextAssistent + 1;
            }

            nextAssistent = arr[(dim * dim) + 2 * dim + 3];

            if (nextAssistent == 35) {
                for (var i = dim * dim; i < (dim * dim) + 2 * dim + 1; i++) {

                    arr[i].alpha = 0.4;

                }

                spielerText.text = l("%euleTippErsteFehlersuche");
				if (arr === MEMORY1) { //Anhand des Arrays möglich
					contain.removeChild(RICHTIGZETTEL[0]);
					contain.removeChild(FALSCHZETTEL[0]);
				} else if (arr === MEMORY2) {
					contain.removeChild(RICHTIGZETTEL[1]);
					contain.removeChild(FALSCHZETTEL[1]);
				} else if (arr === MEMORY3) {
					contain.removeChild(RICHTIGZETTEL[2]);
					contain.removeChild(FALSCHZETTEL[2]);
				} else if (arr === MEMORY4) {
					contain.removeChild(FALSCHZETTEL[3]);
					contain.removeChild(FALSCHZETTEL[3]);
				}
				
				if(arr == MEMORY1) {
                        i = 0;
                    } else if (arr == MEMORY2) {
                        i = 1;
                    } else if (arr == MEMORY3) {
                        i = 2;
                    } else if (arr == MEMORY4) {
                        i = 3;
                    }

                    aufgabentext[i].text = l("%aufgabeErsteFehlersuche");

                arr[fehlerPosition].addEventListener("pressup", function(event) {

                    if(spieleranzahl==1){
						spielerText.text = l("%euleRichtig");
					} else{
						spielerText.text = l("%euleRichtigWarten");
					}
                if(arr == MEMORY1) {
                        i = 0;
                    } else if (arr == MEMORY2) {
                        i = 1;
                    } else if (arr == MEMORY3) {
                        i = 2;
                    } else if (arr == MEMORY4) {
                        i = 3;
                    }

                    aufgabentext[i].text = l("%aufgabeWarten");
                
                 	var richtig = new createjs.Text(l("%richtig"), boxheight + "px Arial black", "white");
                 	richtig.x = startX + event.target.x + boxwidth / 2 - richtig.getMeasuredWidth() / 2;
                    richtig.y = startY + event.target.y - boxwidth / 4;
                    contain.addChild(richtig);
                    
                    alleFertig++;
                    if (alleFertig == spieleranzahl) {
                        modus = 3;
                        alleFertig = 0;
                        rankingCount = 0;
                        setTimeout(function() {splitScreen(spieleranzahl)}, 2000);
                    }

                    arr[fehlerPosition].removeAllEventListeners();

                });




            }


        }

        arr[dim * dim + 2 * dim].alpha = 0.4;
    });


    contain.addChild(RICHTIGZETTEL[j], FALSCHZETTEL[j]);



}

function geradeUngerade(startX, startY, dim, orient, arr, arr2, farbSchema){

	if (arr === MEMORY1) { //Anhand des Arrays möglich
		var SpielerName = "Spieler 1";
		var anzahlFehler = fehlerP1;
		var nummer = 0;
	} else if (arr === MEMORY2) {
		var SpielerName = "Spieler 2";
		var anzahlFehler = fehlerP2;
		var nummer = 1;
	} else if (arr === MEMORY3) {
		var SpielerName = "Spieler 3";
		var anzahlFehler = fehlerP3;
		var nummer = 2;
	} else if (arr === MEMORY4) {
		var SpielerName = "Spieler 4";
		var anzahlFehler = fehlerP4;
		var nummer = 3;
	}
	
	var spalten = dim;
	
	var contain = new createjs.Container();
    var id;
    var spielerText = new createjs.Text("", "14px Arial Black", "navy");
	
    var gerade = 0;
    var zufall = Math.floor(Math.random() * 2);

    var textFrage = new createjs.Text(l("%frageGeradeUngerade"), schriftGroesse + "px Arial Black", "white");
    textFrage.x = startX;
    textFrage.y = startY + (boxwidth + abstand);

	if (zufall == 0) {
		var textGerade = new createjs.Text(l("%gerade"), schriftGroesse + "px Arial Black", '#009999');               
    } else {
		var textGerade = new createjs.Text(l("%ungerade"), schriftGroesse + "px Arial Black", '#595959');
    }
	
	textGerade.shadow = new createjs.Shadow("white", 1, 1, 8);
    textGerade.x = startX + textFrage.getMeasuredWidth();
	textGerade.y = startY + (boxwidth + abstand);

    var star0 = new createjs.Bitmap("img/star0.png");
    var star1 = new createjs.Bitmap("img/star1.png");
    var star2 = new createjs.Bitmap("img/star2.png");
    var star3 = new createjs.Bitmap("img/star3.png");

    if (FERTIG[nummer] == 0) {
		star0.x = startX + (boxwidth + abstand) * (2);
        star0.y = startY + (boxheight + abstand) * (3);
        contain.addChild(star0);
    } else if (FERTIG[nummer] == 1) {
        star1.x = startX + (boxwidth + abstand) * (2);
        star1.y = startY + (boxheight + abstand) * (3);
		contain.addChild(star1);
    } else if (FERTIG[nummer] == 2) {
        star2.x = startX + (boxwidth + abstand) * (2);
        star2.y = startY + (boxheight + abstand) * (3);
        contain.addChild(star2);
    } else {
        star3.x = startX + (boxwidth + abstand) * (2);
        star3.y = startY + (boxheight + abstand) * (3);
        contain.addChild(star3);
    }

    contain.addChild(textFrage);
    contain.addChild(textGerade);
    eule(arr, contain, startX, startY, anzahlFehler, spielerText, SpielerName);
    timeIn = (new Date()).getTime() + timeOut;

    for (var i = 0; i < 7; i++) {
        box = new createjs.Shape();
        contain.addChild(box);
        box.id = i;
		box.name = "true";
        randomBit = Math.floor(Math.random() * 2);
			
		if (i == 5) {
            randomBit=0;
        } else if(i==6){
            randomBit=1;
        }
				
        //Verschiedene Farbsets erlauben
        if (farbSchema == 0) { //Default
			box.color = zettelFarben[randomBit];
			shiftFarbe = 0; // Verschiebung im FarbArray
        } else if (farbSchema == 1) {
            box.color = zettelFarben[randomBit + 2];
            shiftFarbe = 2;
        } else if (farbSchema == 2) {
            box.color = zettelFarben[randomBit + 4];
            shiftFarbe = 4;
        } else if (farbSchema == 3) {
            box.color = zettelFarben[randomBit + 6];
            shiftFarbe = 6;
        }
				
		arr.push(box);
        if (orient == 0) {
            box.shadow = new createjs.Shadow("#4C4C4C", 3, 3, 8);
        } else if (orient == 90) {
            box.shadow = new createjs.Shadow("#4C4C4C", -3, 3, 8);
        } else if (orient == 180) {
            box.shadow = new createjs.Shadow("#4C4C4C", -3, -3, 8);
        } else if (orient == 270) {
            box.shadow = new createjs.Shadow("#4C4C4C", 3, -3, 8);
        }    				
				
		box.graphics.setStrokeStyle(2).beginFill(box.color).drawRoundRect(startX, startY, boxwidth, boxheight, 6, 6).endFill();				
			
		if (i < 5) {
            if (randomBit == 0) {
				gerade += 1;
            }                
			box.x = (boxwidth + abstand) * (i % spalten);
            box.y = (boxheight + abstand) * (i / spalten | 0);
        } else {
            if (i == 5) {
                box.x = (boxwidth + abstand) * (1);
                box.y = (boxheight + abstand) * (2);
			} else {
                box.x = (boxwidth + abstand) * (3);
                box.y = (boxheight + abstand) * (2);
			}
            box.addEventListener("pressup", function(event) {
				for (var z = 5; z < 7; z++) {
					arr[z].removeAllEventListeners();
				}
				for (var j = 0; j < 7; j++) {
					if(gerade % 2 == zufall && arr[j].color != arr[5].color){
						arr[j].alpha = 0.2;									
					}
					if(gerade % 2 != zufall && arr[j].color != arr[6].color){
						arr[j].alpha = 0.2;
					}	
				}
							
				if ((gerade % 2 == zufall && event.target.id==5 )|| (gerade % 2 != zufall && event.target.id==6 )) {
					FERTIG[nummer] = FERTIG[nummer] + 1;
					var feedback = new createjs.Text(l("%richtig"), boxheight + "px Arial black", "white");
					spielerText.text = l("%euleRichtig");
					if (FERTIG[nummer] == 3) {
						star3.x = startX + (boxwidth + abstand) * (2);
						star3.y = startY + (boxheight + abstand) * (3);
						contain.addChild(star3);
						ranking(startX, startY, arr, contain, SpielerName, spielerText);
						if (alleFertig == spieleranzahl) {
							alleFertig = 0;
							modus = 1;
							rankingCount = 0;
							anzahlFehler = 0;
							setTimeout(function() {
								splitScreen(spieleranzahl)
							}, 2000);
						}
					} else {
						setTimeout(function() {
							neuesFeld(arr, contain, startX, startY, anzahlFehler, spielerText, SpielerName)
						}, 1000);
					}								
				} else {
					var feedback = new createjs.Text(l("%falsch"), boxheight + "px Arial black", "white");
					setTimeout(function() {
						neuesFeld(arr, contain, startX, startY, anzahlFehler, spielerText, SpielerName)
					}, 3000);
				}
								
				feedback.x = startX + event.target.x + boxwidth / 2 - feedback.getMeasuredWidth() / 2;
				feedback.y = startY + event.target.y - boxwidth / 4;
				contain.addChild(feedback);
				timeIn = (new Date()).getTime() + timeOut;  
				contain.addChild(spielerText);
			});
		}
	}
    contain.x = startX + dim * (boxwidth + abstand) / 2;
    contain.y = startY + dim * (boxwidth + abstand) / 2;
    contain.regX = startX + dim * (boxwidth + abstand) / 2;
    contain.regY = startY + dim * (boxheight + abstand) / 2;
    contain.rotation = orient;
	stage.addChild(contain);
    //stage.update();
}

function endscreen(startX, startY, dim, orient, arr, arr2, farbSchema){
    var textE1 = new createjs.Text(l("%start"), "30px Arial Black", "white");
        textE1.x = (1 / 4) * (stage.canvas.width) - textE1.getMeasuredWidth() / 2;
        textE1.y = 750;


        var textE2 = new createjs.Text(l("%fehlersuche"), "30px Arial Black", "white");
        textE2.x = (3 / 4) * (stage.canvas.width) - textE2.getMeasuredWidth() / 2;
        textE2.y = 750;

        var textE3 = new createjs.Text(l("%einfuehrung"), "30px Arial Black", "white");
        textE3.x = (2 / 4) * (stage.canvas.width) - textE3.getMeasuredWidth() / 2;
        textE3.y = 750;


        var startscreen = new createjs.Bitmap("img/startscreen.png");
        startscreen.x = (1 / 4) * (stage.canvas.width) - 128;
        startscreen.y = 600;
        startscreen.addEventListener("pressup", function() {
            window.location.reload();

        });
        var fehlersuche = new createjs.Bitmap("img/fehlersuche.png");
        fehlersuche.x = (3 / 4) * (stage.canvas.width) - 128;
        fehlersuche.y = 600;
        fehlersuche.addEventListener("pressup", function() {
            modus = 3;
            splitScreen(spieleranzahl);


        });
        var einfuehrung = new createjs.Bitmap("img/einfuehrung.png");
        einfuehrung.x = (2 / 4) * (stage.canvas.width) - 128;
        einfuehrung.y = 600;
        einfuehrung.addEventListener("pressup", function() {
            modus = 1;
            splitScreen(spieleranzahl);

        });

        var title = new createjs.Text(l("%pruefungBestanden"), "50px Arial Black", "white");
        title.x = (stage.canvas.width - title.getMeasuredWidth()) / 2;
        title.y += 50;
        title.shadow = new createjs.Shadow("#000000", 5, 5, 10);

        if (spieleranzahl == 1) {
            RANK[0] = "Spieler 1";
        }

        BERWERTUNG = [];
        BERWERTUNG = [l("%ersterPlatz"), l("%zweiterPlatz"), l("%dritterPlatz"), l("%vierterPlatz")];
        STERNE = [];
        STERNE = [l("%ersterPlatzSterne"), l("%zweiterPlatzSterne"), l("%dritterPlatzSterne"), l("%vierterPlatzSterne")];

        var scoreBG = new createjs.Bitmap("img/scoreBG6.png");
        scoreBG.x = title.x;

        var scoreText = new createjs.Text("", "28px Arial Black", '#b20000');
        scoreText.x = scoreBG.x + 150;
        scoreText.y = 200;

        var p1 = new createjs.Bitmap("img/p1.png");
        var p2 = new createjs.Bitmap("img/p2.png");
        var p3 = new createjs.Bitmap("img/p3.png");
        var p4 = new createjs.Bitmap("img/p4.png");
        var eule1 = new createjs.Bitmap("img/eule1.png");
        p1.visible = p2.visible = p3.visible = p4.visible = eule1.visible = false;

        if (spieleranzahl == 1) {
            //p1.visible = true;
            //p1.x = scoreText.x+(scoreBG.x/2);
            //p1.y = scoreText.y+50;
            eule1.visible = true;
            eule1.x = scoreText.x + (scoreBG.x / 2);
            eule1.y = scoreText.y + 50;
            RANK[0] = " ";

        }

        for (var i = 0; i < RANK.length; i++) {
            if (spieleranzahl == 1) {
                scoreText.text += l("%glueckwunsch");
                scoreText.text += RANK[i];
                scoreText.text += " ";
                break;
            } else {
                scoreText.text += BERWERTUNG[i] + " : ";
                //scoreText.text += RANK[i];
                if (RANK[i] == "Spieler 1") {
                    p1.visible = true;
                    //p1.x = scoreText.x+(scoreBG.x/2)-40;
                    p1.x = stage.canvas.width / 2;
                    p1.y = scoreText.y - 20 + i * 70;
                } else if (RANK[i] == "Spieler 2") {
                    p2.visible = true;
                    //p2.x = scoreText.x+(scoreBG.x/2)-40;
                    p2.x = stage.canvas.width / 2;
                    p2.y = scoreText.y - 20 + i * 70;
                } else if (RANK[i] == "Spieler 3") {
                    p3.visible = true;
                    //p3.x = scoreText.x+(scoreBG.x/2)-40;
                    p3.x = stage.canvas.width / 2;
                    p3.y = scoreText.y - 20 + i * 70;
                } else if (RANK[i] == "Spieler 4") {
                    p4.visible = true;
                    //p4.x = scoreText.x+(scoreBG.x/2)-40;
                    p4.x = stage.canvas.width / 2;
                    p4.y = scoreText.y - 20 + i * 70;
                }

                scoreText.text += "\n";
                scoreText.text += STERNE[i];
                scoreText.text += "\n";

            }
        }


        stage.addChild(textE1, textE2, textE3, startscreen, fehlersuche, einfuehrung, title, scoreBG, scoreText, p1, p2, p3, p4, eule1);

}

function fehlersuche(startX, startY, dim, orient, arr, arr2, farbSchema,contain){
    box.addEventListener("pressup", function(event) {


                        if (arr === MEMORY1) { //Anhand des Arrays möglich
                            var SpielerName = "Spieler 1";
                            var anzahlFehler = fehlerP1;
                            var nummer = 0;
                        } else if (arr === MEMORY2) {
                            var SpielerName = "Spieler 2";
                            var anzahlFehler = fehlerP2;
                            var nummer = 1;
                        } else if (arr === MEMORY3) {
                            var SpielerName = "Spieler 3";
                            var anzahlFehler = fehlerP3;
                            var nummer = 2;
                        } else if (arr === MEMORY4) {
                            var SpielerName = "Spieler 4";
                            var anzahlFehler = fehlerP4;
                            var nummer = 3;
                        }
                        timeIn = (new Date()).getTime() + timeOut;

                        //Gehe durch alle erzeugten Felder (ohne Assistent)
                        for (var p = 0; p < dim * dim; p++) {

                            //Existiert Boxobjekt im Array welcher als fehlerStelle (durch fehlerEinbauen()) markiert wurde?
                            if (arr[p].name == "false") {
                                //touched TargetId == FehlerBox Id? <=> RICHTIG ausgewählt!
                                if (event.target.id == arr[p].id) {
                                    // arr[p].graphics.setStrokeStyle(4).beginStroke('white').beginFill("green").drawRoundRect(startX, startY, boxwidth, boxheight, 6, 6).endFill();
                                    //stage.update();

                                    contain.addChild(arr[event.target.id]);


                                    var richtig = new createjs.Text(l("%richtig"), boxheight + "px Arial black", "white");
                                    //richtig.outline = 1;
                                    richtig.x = startX + event.target.x + boxwidth / 2 - richtig.getMeasuredWidth() / 2;
                                    richtig.y = startY + event.target.y - boxwidth / 4;
                                    contain.addChild(richtig);
                                    spielerText.text = l("%euleRichtig");
                                    //stage.update();



                                    //Da richtig gelegen, können die anderen EventListener entfert werden
                                    for (var m = 0; m < dim * dim; m++) {
                                        arr[m].removeAllEventListeners("pressup");
                                    }

                                    
                                    ranking(startX, startY, arr, contain, SpielerName, spielerText);
                                    if (alleFertig == spieleranzahl) {
                                            alleFertig = 0;
                                            modus = 4;
                                            rankingCount = 0;
                                            anzahlFehler = 0;
                                            setTimeout(function() {
                                                splitScreen(spieleranzahl)
                                            }, 2000);
                                    }




                                    //touched TargetId != FehlerBox Id? => FALSCH ausgewählt!    
                                } else {
                                    //arr[event.target.id].graphics.beginFill(event.target.color).beginStroke("orange").setStrokeStyle(5).drawRoundRect(startX, startY, boxwidth, boxheight, 6, 6).endFill();
                                    event.target.removeAllEventListeners("pressup");
                                    var falsch = new createjs.Text(l("%falsch"), boxheight + "px Arial black", "white");
                                    // falsch.outline = 1;
                                    //Abhängig von 1/2/3/4 Spieler (insb. boxwidth/boxheight)
                                    falsch.x = startX + event.target.x + boxwidth / 2 - falsch.getMeasuredWidth() / 2;
                                    falsch.y = startY + event.target.y - boxwidth / 4;

                                    anzahlFehler += 1;
                                    if (anzahlFehler == 1) {


                                        var herzP1 = new createjs.Bitmap("img/heart2.png");
                                        var eulenabstand = 150 + stage.canvas.width / 34;
                                        if (arr == MEMORY1 && spieleranzahl == 3) {
                                            herzP1.x = startX - 10 + 3 * (boxwidth + abstand) + abstand - 75 - stage.canvas.width / 21;
                                            herzP1.y = startY + 8.5 * boxheight;
                                        } else {
                                            //herzP1.x = startX-(eulenabstand+10);
                                            herzP1.x = startX - eulenabstand;
                                            herzP1.y = startY + 0.5 * boxheight;
                                        }

                                        contain.addChild(herzP1);


                                    } else if (anzahlFehler == 2) {
                                        var herzP1 = new createjs.Bitmap("img/heart1.png");
                                        var eulenabstand = 150 + stage.canvas.width / 34;
                                        if (arr == MEMORY1 && spieleranzahl == 3) {
                                            herzP1.x = startX - 10 + 3 * (boxwidth + abstand) + abstand - 75 - stage.canvas.width / 21;
                                            herzP1.y = startY + 8.5 * boxheight;
                                        } else {
                                            //herzP1.x = startX-(eulenabstand+10);
                                            herzP1.x = startX - eulenabstand;
                                            herzP1.y = startY + 0.5 * boxheight;
                                        }
                                        contain.addChild(herzP1);
                                    } else if (anzahlFehler == 3) {
                                        var herzP1 = new createjs.Bitmap("img/heart0.png");
                                        var eulenabstand = 150 + stage.canvas.width / 34;
                                        if (arr == MEMORY1 && spieleranzahl == 3) {
                                            herzP1.x = startX - 10 + 3 * (boxwidth + abstand) + abstand - 75 - stage.canvas.width / 21;
                                            herzP1.y = startY + 8.5 * boxheight;
                                        } else {
                                            //herzP1.x = startX-(eulenabstand+10);
                                            herzP1.x = startX - eulenabstand;
                                            herzP1.y = startY + 0.5 * boxheight;
                                        }

                                        contain.addChild(herzP1);

                                    }
                                    //spielerText.text = "Hmm.. der Zettel war es nicht, versuchs nochmal "+SpielerName;
                                    //spielerText.x = startX;
                                    //spielerText.y = startY-30;
                                    contain.addChild(arr[event.target.id], spielerText);
                                    event.target.removeAllEventListeners("pressup");
                                    contain.addChild(falsch);
                                    //stage.update();

                                    eule(arr, contain, startX, startY, anzahlFehler, spielerText, SpielerName);

                                }
                            }
                        }
                        //stage.update();
                    });
}
